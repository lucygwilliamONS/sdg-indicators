---
title: About
permalink: /about/
layout: page
---
The [Sustainable Development Goals](http://www.un.org/sustainabledevelopment/sustainable-development-goals/) (SDGs) are a set of ambitious goals and targets designed to make the world a better place by ending poverty, halting climate change and reducing inequalities. This work programme is part of the country’s commitment to the agenda 2030 on sustainable development. 

ONS are fulfilling the mandate placed on all NSIs by the UN General Assembly to coordinate and report on the global SDG indicators. We aim to shine a light on who is being ‘left behind’ so policy makers can take action, and we aim to support colleagues across the world develop capability. Cabinet Office and DfID have responsibility for policy implementation. Our job is to collaborate and coordinate with topic leads across the GSS and beyond, using innovative techniques and to promote the work we do as an office that relates to SDGs. We are one of the world leaders in providing data to monitor progress. 


The site is based on the open source '[Open SDG](https://open-sdg.readthedocs.io/en/latest/)' platform. The Open SDG platform is the result of collaboaration between the US government, the UK [Office For National Statistics](https://www.ons.gov.uk/) (ONS), and the nonprofit [Center For Open Data Enterprise](http://opendataenterprise.org/) (CODE).

Other countries wishing to reuse the Open SDG reporting platform are welcome to do so for free. The Open SDG [Quickstart guide](https://open-sdg.readthedocs.io/en/latest/quick-start/) gives technical instructions on the quickest way to get a copy of the Open SDG platform up and running. Note that this site is currently at an Alpha stage, and so is still under development. 

Our [Guidance page](https://github.com/ONSdigital/sdg-indicators/wiki/Clone-your-own%3A-step-by-step-instructions-%28Windows%29) also documents this process in more detail for first-time users.

If you have any feedback on this website or UK SDG data then please contact us at <a href="mailto:{{site.email_contacts.questions}}">{{site.email_contacts.questions}}</a>. The answers to some frequently asked questions are also available in the [Frequently Asked Questions]({{ site.baseurl }}{% link _pages/faq.md %}).
